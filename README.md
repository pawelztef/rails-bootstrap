Rails Bootstrap
================

Ruby on Rails
-------------

This application requires:

- Ruby 2.4.1
- Rails 5.1.6

Learn more about [Installing Rails](http://railsapps.github.io/installing-rails.html).

Getting Started
---------------
Instagram test/sandbox account
login: testomnibus
password: testomnibus